using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeExplosion : MonoBehaviour
{
    [SerializeField] float radius = 5.0f;
    [SerializeField] float power = 10.0f;
    [SerializeField] GameObject explosionPlacement;

    Vector3 positionExplosion = new Vector3(0, 0, 0);
    Collider[] colliders;

    void Start()
    {
        //Set la position de l'explosion
        positionExplosion = explosionPlacement.transform.position;

        //prend tout les collider dans qui se font toucher par la sphere d'explosion
        colliders = Physics.OverlapSphere(positionExplosion, radius);

        //for tout les collider dans le tableau de colliders applique la force de l'explosion sur le rigidbody
        foreach(Collider Collider in colliders)
        {
            Rigidbody rb = Collider.GetComponentInChildren<Rigidbody>();

            if(rb != null)
            {
                rb.AddExplosionForce(power, positionExplosion, radius, 3.0f);
            }
        }
    }

}
