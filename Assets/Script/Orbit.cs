using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit : MonoBehaviour
{
    [SerializeField] float minSpeedRotation;
    [SerializeField] float maxSpeedRotation;
    [SerializeField] float distanceAstre;
    [SerializeField] bool randomOrbit;
    [SerializeField] Transform astre;


    float speedRotation;
    float rotation = 0;

    int direction;

    Vector3 distance = new Vector3(0, 0, 0);

    void Start()
    {
        SetDistanceAstre();
        SetInitialRotation();
        SetRandomSpeedRotationAndDirection();
        
    }

    void Update()
    {
        Rotation();
    }

    void SetRandomSpeedRotationAndDirection()
    {
        direction = Random.Range(0, 2);

        if(direction == 0)
        {
            speedRotation = Random.Range(minSpeedRotation, maxSpeedRotation);
        }
        else
        {
            speedRotation = Random.Range(-minSpeedRotation, -maxSpeedRotation);
        }
    }

    void Rotation()
    {
        transform.Rotate(new Vector3(0, 0, rotation));
        rotation = speedRotation * Time.deltaTime;
    }

    void SetDistanceAstre()
    {
        distance = astre.position;
        distance.x += distanceAstre;
        astre.position = distance;
    }

    void SetInitialRotation()
    {
        int rota;

        if(randomOrbit)
        {
            rota = Random.Range(0, 360);
            transform.Rotate(new Vector3(0, 0, rota));
            rota = Random.Range(0, 360);
            transform.Rotate(new Vector3(0, rota, 0));
            rota = Random.Range(0, 360);
            transform.Rotate(new Vector3(rota, 0, 0));
        }
        else
        {
            transform.Rotate(new Vector3(90, 0, 0));
        }
    }

}
