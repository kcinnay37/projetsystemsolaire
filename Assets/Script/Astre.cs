using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astre : MonoBehaviour
{
    [SerializeField] float maxSpeedRotation;
    [SerializeField] float minSpeedRotation;
    [SerializeField] float scale;
    [SerializeField] Transform orbit;
    [SerializeField] Transform corp;

    float speedRotation;

    Vector3 scales = new Vector3(1, 1, 1);

    void Start()
    {
        SetScale();
        SetInitialRotation();
        SetRandomSpeedRotationAndDirection();
    }

    void Update()
    {
        Rotation();
        FollowOrbit();
    }

    void SetInitialRotation()
    {
        int rota;

        rota = Random.Range(0, 360);
        corp.transform.Rotate(new Vector3(0, 0, rota));
        rota = Random.Range(0, 360);
        corp.transform.Rotate(new Vector3(0, rota, 0));
        rota = Random.Range(0, 360);
        corp.transform.Rotate(new Vector3(rota, 0, 0));
    }

    void SetRandomSpeedRotationAndDirection()
    {
        int direction = Random.Range(0, 2);

        if (direction == 0)
        {
            speedRotation = Random.Range(minSpeedRotation, maxSpeedRotation);
        }
        else
        {
            speedRotation = Random.Range(-minSpeedRotation, -maxSpeedRotation);
        }
    }

    void Rotation()
    {
        corp.transform.Rotate(new Vector3(0, 0, speedRotation * Time.deltaTime));
    }

    void SetScale()
    {
        scales = new Vector3(scale, scale, scale);
        corp.transform.localScale = scales;
    }

    void FollowOrbit()
    {
        orbit.transform.position = transform.position;
        corp.transform.position = transform.position;
    }
}
